﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArqsiArmario.DTOs
{
    public class ProdutoPartesDto
    {
        public ProdutoPartesDto(ProdutoDto ProdutoPai, ProdutoDto ProdutoFilho)
        {
            this.ProdutoPai = ProdutoPai;
            this.ProdutoFilho = ProdutoFilho;
        }
        public ProdutoPartesDto() { }

        public int? ProdutoPaiId { get; set; }
        public ProdutoDto ProdutoPai { get; set; }

        public int? ProdutoFilhoId { get; set; }
        public ProdutoDto ProdutoFilho { get; set; }

    }
}
