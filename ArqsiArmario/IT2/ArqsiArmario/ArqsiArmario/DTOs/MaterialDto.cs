using System.Collections.Generic;
namespace ArqsiArmario.DTOs
{
    public class MaterialDto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public IEnumerable<AcabamentoDto> MaterialAcabamento { get; set; }
        public IEnumerable<ProdutoDto> ProdutoMaterial { get; set; }
        public MaterialDto() { }
        public MaterialDto(string Nome, IEnumerable<AcabamentoDto> MaterialAcabamento, IEnumerable<ProdutoDto> produtoMaterialDtos)
        {
            this.Nome = Nome;
            this.MaterialAcabamento = MaterialAcabamento;
            this.ProdutoMaterial = produtoMaterialDtos;
        }
    }
}