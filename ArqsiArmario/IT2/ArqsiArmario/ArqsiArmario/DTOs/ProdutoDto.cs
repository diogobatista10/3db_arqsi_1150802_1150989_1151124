using System.Collections.Generic;

namespace ArqsiArmario.DTOs
{
    public class ProdutoDto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public bool Composto { get; set; }
        public ICollection<ProdutoPartesDto> ProdutoPartes { get; set; }

        public ICollection<ProdutoMaterialDto> ProdutoMateriais{ get; set; }
      
        public CategoriaDto Categoria { get; set; }
        public int? CategoriaId { get; set; }
        public DimensaoDto Dimensao { get; set; }
        public RestricaoDto restricao { get; set; }
        public ProdutoDto() { }
        public ProdutoDto(string Nome, bool Composto, ICollection<ProdutoPartesDto> Produtos, ICollection<ProdutoMaterialDto> Material, CategoriaDto Categoria,RestricaoDto Restricao)
        {
            this.Nome = Nome;
            this.Composto = Composto;
            this.ProdutoPartes = Produtos;
            this.ProdutoMateriais = Material;
            this.Categoria = Categoria;
            this.restricao = Restricao;
        }
    }
}