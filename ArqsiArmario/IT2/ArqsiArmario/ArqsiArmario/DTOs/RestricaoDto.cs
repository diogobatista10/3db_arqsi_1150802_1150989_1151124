﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArqsiArmario.DTOs
{
    public class RestricaoDto
    {
        public float min { get; set; }
        public float max { get; set; }
        public bool PM { get; set; }
        public bool MA { get; set; }

        public RestricaoDto() { }
        public RestricaoDto(float Min, float Max, bool PM, bool MA)
        {
            this.min = Min;
            this.max = Max;
            this.PM = PM;
            this.MA = MA;
        }
    }
}

