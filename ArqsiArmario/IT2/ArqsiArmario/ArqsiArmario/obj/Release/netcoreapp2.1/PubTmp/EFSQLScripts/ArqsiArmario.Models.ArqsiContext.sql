﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE TABLE [Acabamentos] (
        [Id] int NOT NULL IDENTITY,
        [Nome] nvarchar(max) NULL,
        CONSTRAINT [PK_Acabamentos] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE TABLE [Categorias] (
        [Id] int NOT NULL IDENTITY,
        [Composto] bit NOT NULL,
        [Nome] nvarchar(max) NULL,
        [Descricao] nvarchar(max) NULL,
        [CategoriaId] int NULL,
        CONSTRAINT [PK_Categorias] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Categorias_Categorias_CategoriaId] FOREIGN KEY ([CategoriaId]) REFERENCES [Categorias] ([Id]) ON DELETE NO ACTION
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE TABLE [DimensaoDC] (
        [Id] int NOT NULL IDENTITY,
        [AlturaMin] real NOT NULL,
        [AlturaMax] real NOT NULL,
        CONSTRAINT [PK_DimensaoDC] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE TABLE [Materiais] (
        [Id] int NOT NULL IDENTITY,
        [Nome] nvarchar(max) NULL,
        CONSTRAINT [PK_Materiais] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE TABLE [Dimensoes] (
        [Id] int NOT NULL IDENTITY,
        [AlturaId] int NULL,
        [ProfundidadeId] int NULL,
        [LarguraId] int NULL,
        CONSTRAINT [PK_Dimensoes] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Dimensoes_DimensaoDC_AlturaId] FOREIGN KEY ([AlturaId]) REFERENCES [DimensaoDC] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Dimensoes_DimensaoDC_LarguraId] FOREIGN KEY ([LarguraId]) REFERENCES [DimensaoDC] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Dimensoes_DimensaoDC_ProfundidadeId] FOREIGN KEY ([ProfundidadeId]) REFERENCES [DimensaoDC] ([Id]) ON DELETE NO ACTION
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE TABLE [Valor] (
        [Id] int NOT NULL IDENTITY,
        [ValorDiscreto] int NOT NULL,
        [DimensaoDCId] int NULL,
        CONSTRAINT [PK_Valor] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Valor_DimensaoDC_DimensaoDCId] FOREIGN KEY ([DimensaoDCId]) REFERENCES [DimensaoDC] ([Id]) ON DELETE NO ACTION
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE TABLE [MaterialAcabamentos] (
        [MaterialId] int NOT NULL,
        [AcabamentoId] int NOT NULL,
        CONSTRAINT [PK_MaterialAcabamentos] PRIMARY KEY ([MaterialId], [AcabamentoId]),
        CONSTRAINT [FK_MaterialAcabamentos_Acabamentos_AcabamentoId] FOREIGN KEY ([AcabamentoId]) REFERENCES [Acabamentos] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_MaterialAcabamentos_Materiais_MaterialId] FOREIGN KEY ([MaterialId]) REFERENCES [Materiais] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE TABLE [Produtos] (
        [Id] int NOT NULL IDENTITY,
        [Composto] bit NOT NULL,
        [Nome] nvarchar(max) NULL,
        [CategoriaId] int NULL,
        [DimensaoId] int NULL,
        CONSTRAINT [PK_Produtos] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Produtos_Categorias_CategoriaId] FOREIGN KEY ([CategoriaId]) REFERENCES [Categorias] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_Produtos_Dimensoes_DimensaoId] FOREIGN KEY ([DimensaoId]) REFERENCES [Dimensoes] ([Id]) ON DELETE NO ACTION
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE TABLE [ProdutoMaterials] (
        [ProdutoId] int NOT NULL,
        [MaterialId] int NOT NULL,
        CONSTRAINT [PK_ProdutoMaterials] PRIMARY KEY ([ProdutoId], [MaterialId]),
        CONSTRAINT [FK_ProdutoMaterials_Materiais_MaterialId] FOREIGN KEY ([MaterialId]) REFERENCES [Materiais] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_ProdutoMaterials_Produtos_ProdutoId] FOREIGN KEY ([ProdutoId]) REFERENCES [Produtos] ([Id]) ON DELETE CASCADE
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE TABLE [ProdutoPartes] (
        [ProdutoPaiId] int NOT NULL,
        [ProdutoFilhoId] int NOT NULL,
        [ProdutoFilhoId1] int NULL,
        CONSTRAINT [PK_ProdutoPartes] PRIMARY KEY ([ProdutoPaiId], [ProdutoFilhoId]),
        CONSTRAINT [FK_ProdutoPartes_Produtos_ProdutoFilhoId] FOREIGN KEY ([ProdutoFilhoId]) REFERENCES [Produtos] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_ProdutoPartes_Produtos_ProdutoFilhoId1] FOREIGN KEY ([ProdutoFilhoId1]) REFERENCES [Produtos] ([Id]) ON DELETE NO ACTION
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE INDEX [IX_Categorias_CategoriaId] ON [Categorias] ([CategoriaId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE INDEX [IX_Dimensoes_AlturaId] ON [Dimensoes] ([AlturaId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE INDEX [IX_Dimensoes_LarguraId] ON [Dimensoes] ([LarguraId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE INDEX [IX_Dimensoes_ProfundidadeId] ON [Dimensoes] ([ProfundidadeId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE INDEX [IX_MaterialAcabamentos_AcabamentoId] ON [MaterialAcabamentos] ([AcabamentoId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE INDEX [IX_ProdutoMaterials_MaterialId] ON [ProdutoMaterials] ([MaterialId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE INDEX [IX_ProdutoPartes_ProdutoFilhoId] ON [ProdutoPartes] ([ProdutoFilhoId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE INDEX [IX_ProdutoPartes_ProdutoFilhoId1] ON [ProdutoPartes] ([ProdutoFilhoId1]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE INDEX [IX_Produtos_CategoriaId] ON [Produtos] ([CategoriaId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE INDEX [IX_Produtos_DimensaoId] ON [Produtos] ([DimensaoId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    CREATE INDEX [IX_Valor_DimensaoDCId] ON [Valor] ([DimensaoDCId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20181109212217_InitialCreate')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20181109212217_InitialCreate', N'2.1.3-rtm-32065');
END;

GO

