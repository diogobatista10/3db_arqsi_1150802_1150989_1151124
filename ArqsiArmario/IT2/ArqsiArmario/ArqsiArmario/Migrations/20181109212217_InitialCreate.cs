﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ArqsiArmario.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Acabamentos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Acabamentos", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categorias",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Composto = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    Descricao = table.Column<string>(nullable: true),
                    CategoriaId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categorias", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Categorias_Categorias_CategoriaId",
                        column: x => x.CategoriaId,
                        principalTable: "Categorias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "DimensaoDC",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AlturaMin = table.Column<float>(nullable: false),
                    AlturaMax = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DimensaoDC", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Materiais",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nome = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Materiais", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Dimensoes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AlturaId = table.Column<int>(nullable: true),
                    ProfundidadeId = table.Column<int>(nullable: true),
                    LarguraId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dimensoes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Dimensoes_DimensaoDC_AlturaId",
                        column: x => x.AlturaId,
                        principalTable: "DimensaoDC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Dimensoes_DimensaoDC_LarguraId",
                        column: x => x.LarguraId,
                        principalTable: "DimensaoDC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Dimensoes_DimensaoDC_ProfundidadeId",
                        column: x => x.ProfundidadeId,
                        principalTable: "DimensaoDC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Valor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ValorDiscreto = table.Column<int>(nullable: false),
                    DimensaoDCId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Valor", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Valor_DimensaoDC_DimensaoDCId",
                        column: x => x.DimensaoDCId,
                        principalTable: "DimensaoDC",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MaterialAcabamentos",
                columns: table => new
                {
                    MaterialId = table.Column<int>(nullable: false),
                    AcabamentoId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaterialAcabamentos", x => new { x.MaterialId, x.AcabamentoId });
                    table.ForeignKey(
                        name: "FK_MaterialAcabamentos_Acabamentos_AcabamentoId",
                        column: x => x.AcabamentoId,
                        principalTable: "Acabamentos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MaterialAcabamentos_Materiais_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "Materiais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Produtos",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Composto = table.Column<bool>(nullable: false),
                    Nome = table.Column<string>(nullable: true),
                    CategoriaId = table.Column<int>(nullable: true),
                    DimensaoId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Produtos", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Produtos_Categorias_CategoriaId",
                        column: x => x.CategoriaId,
                        principalTable: "Categorias",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Produtos_Dimensoes_DimensaoId",
                        column: x => x.DimensaoId,
                        principalTable: "Dimensoes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ProdutoMaterials",
                columns: table => new
                {
                    ProdutoId = table.Column<int>(nullable: false),
                    MaterialId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProdutoMaterials", x => new { x.ProdutoId, x.MaterialId });
                    table.ForeignKey(
                        name: "FK_ProdutoMaterials_Materiais_MaterialId",
                        column: x => x.MaterialId,
                        principalTable: "Materiais",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProdutoMaterials_Produtos_ProdutoId",
                        column: x => x.ProdutoId,
                        principalTable: "Produtos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProdutoPartes",
                columns: table => new
                {
                    ProdutoPaiId = table.Column<int>(nullable: false),
                    ProdutoFilhoId = table.Column<int>(nullable: false),
                    ProdutoFilhoId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProdutoPartes", x => new { x.ProdutoPaiId, x.ProdutoFilhoId });
                    table.ForeignKey(
                        name: "FK_ProdutoPartes_Produtos_ProdutoFilhoId",
                        column: x => x.ProdutoFilhoId,
                        principalTable: "Produtos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProdutoPartes_Produtos_ProdutoFilhoId1",
                        column: x => x.ProdutoFilhoId1,
                        principalTable: "Produtos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Categorias_CategoriaId",
                table: "Categorias",
                column: "CategoriaId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_AlturaId",
                table: "Dimensoes",
                column: "AlturaId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_LarguraId",
                table: "Dimensoes",
                column: "LarguraId");

            migrationBuilder.CreateIndex(
                name: "IX_Dimensoes_ProfundidadeId",
                table: "Dimensoes",
                column: "ProfundidadeId");

            migrationBuilder.CreateIndex(
                name: "IX_MaterialAcabamentos_AcabamentoId",
                table: "MaterialAcabamentos",
                column: "AcabamentoId");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoMaterials_MaterialId",
                table: "ProdutoMaterials",
                column: "MaterialId");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoPartes_ProdutoFilhoId",
                table: "ProdutoPartes",
                column: "ProdutoFilhoId");

            migrationBuilder.CreateIndex(
                name: "IX_ProdutoPartes_ProdutoFilhoId1",
                table: "ProdutoPartes",
                column: "ProdutoFilhoId1");

            migrationBuilder.CreateIndex(
                name: "IX_Produtos_CategoriaId",
                table: "Produtos",
                column: "CategoriaId");

            migrationBuilder.CreateIndex(
                name: "IX_Produtos_DimensaoId",
                table: "Produtos",
                column: "DimensaoId");

            migrationBuilder.CreateIndex(
                name: "IX_Valor_DimensaoDCId",
                table: "Valor",
                column: "DimensaoDCId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MaterialAcabamentos");

            migrationBuilder.DropTable(
                name: "ProdutoMaterials");

            migrationBuilder.DropTable(
                name: "ProdutoPartes");

            migrationBuilder.DropTable(
                name: "Valor");

            migrationBuilder.DropTable(
                name: "Acabamentos");

            migrationBuilder.DropTable(
                name: "Materiais");

            migrationBuilder.DropTable(
                name: "Produtos");

            migrationBuilder.DropTable(
                name: "Categorias");

            migrationBuilder.DropTable(
                name: "Dimensoes");

            migrationBuilder.DropTable(
                name: "DimensaoDC");
        }
    }
}
