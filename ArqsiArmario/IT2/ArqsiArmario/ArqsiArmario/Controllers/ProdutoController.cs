﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using ArqsiArmario.DTOs;
using ArqsiArmario.Models;
using System.Threading.Tasks;
using ArqsiArmario.Repository;
using Microsoft.EntityFrameworkCore;

namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private ArqsiContext context;
        private ICategoriaRepository repCategoria;
        private IDimensaoRepository repDimensao;
        private IProdutoMaterialRepository repMaterial;
        private IMaterialRepository repMat;
        private IProdutoRepository repProduto;
        private IProdutoPartesRepository repPartes;
        
        public ProdutoController(IProdutoRepository produtoRepository, IDimensaoRepository dimensaoRepository, IProdutoMaterialRepository produtoMaterialRepository, ICategoriaRepository categoriaRepository, IProdutoPartesRepository produtoPartesRepository, IMaterialRepository materialRepository, ArqsiContext context)
        {
            this.context = context;
            this.repProduto = produtoRepository;
            this.repDimensao = dimensaoRepository;
            this.repMaterial = produtoMaterialRepository;
            this.repCategoria = categoriaRepository;
            this.repPartes = produtoPartesRepository;
            this.repMat = materialRepository;
        }

        [HttpGet]
        public IEnumerable<Produto> GetProdutos()
        {
           /* IEnumerable<ProdutoDto> ListaProdutosDTO = Enumerable.Empty<ProdutoDto>();
            ProdutoDto aux = new ProdutoDto();
            foreach (Produto produto in repProduto.GetProdutos())
            {
                aux = new ProdutoDto();
                aux.Nome = produto.Nome;
                ListaProdutosDTO = ListaProdutosDTO.Concat(new[] { aux });
            }*/
            return repProduto.GetProdutos();

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetProduto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produto1 = repProduto.GetProdutoByID(id);
            if (produto1 == null)
            {
                return NotFound();
            }

            //ProdutoDto produtoDto = new ProdutoDto();
            //produtoDto.Nome = produto1.Nome;
            //produtoDto.Categoria = produto1.Categoria.toDTO();
            //produtoDto.Composto = produto1.Composto;
            //produtoDto.CategoriaId = produto1.CategoriaId;

            return Ok(produto1);
        }


        // GET: api/Produto/?nome={nome}
        [HttpGet("nome={name}")]
        public IEnumerable<Produto> GetProdutosPorNome([FromRoute] string name)
        {
            var produto = context.Produtos.Include(p => p.Categoria).Include(p => p.ProdutosMateriais).Include(p => p.ProdutosPartes).Where(p => p.Nome.Equals(name, StringComparison.InvariantCultureIgnoreCase));
            return produto;
        }
        // GET: api/Produto/ValidarComponentes
        [HttpGet("ValidarComponentes")]
        public bool ValidarComponentes([FromQuery] int idProduto, [FromQuery] int idMaterial, [FromQuery] int idAcabamento)
        {
            var produto = context.Produtos.Where(p => p.Id == idProduto).
                Include(p => p.Categoria).
                Include(p=>p.Dimensao).
                Include(p => p.ProdutosMateriais).ThenInclude(m=> m.Material).
                Include(p => p.ProdutosPartes).ThenInclude(f=> f.ProdutoFilho).
                SingleOrDefault();

            if (produto == null)
            {
                return false;
            }
            var mat = context.Materiais.Where(m => m.Id == idMaterial).
                Include(m => m.ProdutoMateriais).ThenInclude(p => p.Produto).
                Include(m => m.MaterialAcabamentos).ThenInclude(a => a.Acabamento).
                SingleOrDefault();

            if (mat == null)
            {
                return false;
            }

            var aca = context.Acabamentos.Where(a => a.Id == idAcabamento).
                Include(a => a.MaterialAcabamentos).ThenInclude(m => m.Material).
                SingleOrDefault();
            if (aca == null)
            {
                return false;
            }
            var promat = produto.ProdutosMateriais.Where(pmat => pmat.MaterialId == idMaterial).SingleOrDefault();

            var mataca = mat.MaterialAcabamentos.Where(matac => matac.AcabamentoId == idAcabamento).SingleOrDefault();

            if (promat == null)
            {
                return false;
            }
            else if (mataca == null)
            {
                return false;
            }
            return true;
        }
        // GET: api/Produto/ValidarDimensoes
        [HttpGet("ValidarDimensoes")]
        public bool ValidarDimensoes([FromQuery] int idDimensao, [FromQuery] int idProduto)
        {
            var produto = context.Produtos.Where(p => p.Id == idProduto).
                Include(p => p.Categoria).
                Include(p => p.Dimensao).
                Include(p => p.ProdutosMateriais).
                Include(p => p.ProdutosPartes).
                SingleOrDefault();

            if (produto == null)
            {
                return false;
            }
            var dim = context.Dimensoes.Where(d => d.Id == idDimensao).
                Include(dc => dc.Altura).
                Include(dc => dc.Largura).
                Include(dc => dc.Profundidade).
                SingleOrDefault();
           
            return true;
        }
        [HttpGet("{id}/{Produtos}", Name = "GetProdutoPartes")]
        public async Task<ActionResult<List<ProdutoPartesDto>>> GetProdutoPartes([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produto = repProduto.GetProdutoByID(id);

            if (produto == null)
            {
                return NotFound();
            }

            if (produto.Composto == false) { return Content("Não tem"); }
            else
            {
                List<ProdutoPartesDto> ListaP = new List<ProdutoPartesDto>();
                foreach (ProdutoPartes p in produto.ProdutosPartes)
                {
                    ListaP.Add(p.toDTO());
                }
                return ListaP;
            }

        }

        [HttpGet("{id}/Produtos={Produtos}", Name = "GetProdutoParteEM")]
        public async Task<ActionResult<ICollection<Produto>>> GetProdutosPai([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produto = repProduto.GetProdutoByID(id);

            if (produto == null)
            {
                return NotFound();
            }
            List<Produto> resultado = new List<Produto>();

            foreach (Produto P in repProduto.GetProdutos())
            {
                foreach (ProdutoPartes P1 in P.ProdutosPartes)
                {
                    if (P1.ProdutoFilhoId == produto.Id)
                    {
                        resultado.Add(P);
                    }
                }
            }

            return resultado;
        }


        // POST: api/Produto
        [HttpPost]
        public async Task<IActionResult> PostProduto([FromBody] Produto Produto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            if (Produto.ProdutosPartes == null)
            {
                Produto.Composto = false;
            }
            else
            {
                Produto.Composto = true;
            }
            if (Produto.Composto == true)
            {
                bool auxB = caber(Produto);
                if (!auxB)
                {
                    return Content("Não Cabe!!");
                }
               

            }
            Produto.Categoria = repCategoria.GetCategoriaByID(Produto.CategoriaId);
             Produto.Dimensao = repDimensao.GetDimensoesByID(Produto.DimensaoId);
            //// Produto.ProdutosMateriais.Add(repMaterial.GetMaterialByProduto(Produto));

            //  //var materiais = _context.Materiais.Where(t => produtodto.Materiais.Contains(t.ID));



            //    ICollection<ProdutoMaterial> Lpm = Produto.ProdutosMateriais;
            //    foreach(var pm in Lpm){ 
            //    Material m = repMat.GetMaterialByID(pm.MaterialId);
            //    repMaterial.InsertProdutoMaterial(new ProdutoMaterial(Produto, m));
            //    repMaterial.Save();
            //}

            repProduto.InsertProduto(Produto);
            repProduto.Save();

            return CreatedAtAction("GetProduto", new { id = Produto.Id }, Produto);
        }
        private bool caber(Produto Produto)
        {
            bool auxB = true;
            float ocupadoA = 0;
            float ocupadoL = 0;
            float ocupadoP = 0;
            foreach (ProdutoPartes aux in Produto.ProdutosPartes)
            {
                
                if ((aux.ProdutoFilho.Dimensao.Altura.AlturaMin < Produto.Dimensao.Altura.AlturaMin)
                    && (aux.ProdutoFilho.Dimensao.Largura.AlturaMin < Produto.Dimensao.Largura.AlturaMin)
                    && (aux.ProdutoFilho.Dimensao.Profundidade.AlturaMin < Produto.Dimensao.Profundidade.AlturaMin))
                {
                    ocupadoA += aux.ProdutoFilho.Dimensao.Altura.AlturaMin;
                    ocupadoL += aux.ProdutoFilho.Dimensao.Largura.AlturaMin;
                    ocupadoP += aux.ProdutoFilho.Dimensao.Profundidade.AlturaMin;
                    
                    if (ocupadoA<Produto.Dimensao.Altura.AlturaMin && ocupadoL<Produto.Dimensao.Largura.AlturaMin && ocupadoP<Produto.Dimensao.Profundidade.AlturaMin )
                    {
                        auxB = true;
                    }
                    else
                    {
                        auxB = false;
                    }
                }
                else
                {
                    auxB = false;
                }
            }
            return auxB;
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduto(int id, Produto item)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var temp = repProduto.GetProdutos().Where<Produto>(a => a.Id == id);

            Produto aux = temp.First<Produto>();
            aux.Nome = item.Nome;
            aux.Dimensao = item.Dimensao;
            aux.Categoria = item.Categoria;
            List<ProdutoMaterialDto> ListaPm = new List<ProdutoMaterialDto>();
            /*     foreach (ProdutoMaterial pm in aux.ProdutosMateriais)
                 {
                     ListaPm.Add(pm.toDTO());
                 }
                 List<ProdutoDto> ListaP = new List<ProdutoDto>();
                 foreach (Produto p in aux.Produtos)
                 {
                     ListaP.Add(p.toDTO());
                 }*/

            try
            {
                repProduto.UpdateProduto(aux);
                repProduto.Save();
                return NoContent();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProdutoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }


        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produto = repProduto.GetProdutoByID(id);
            if (produto == null)
            {
                return NotFound();
            }

            repProduto.DeleteProduto(id);
            repProduto.Save();

            return Ok(produto);
        }
        private bool ProdutoExists(int id)
        {
            return repProduto.GetProdutos().Any(e => e.Id == id);
        }
        //restricoes

        //[Route("verificaRestricao/{idProduto:int}/{taxaOcupacao:double}")]
        //public IActionResult VerificarTaxaOcupacao(int idProduto, double taxaOcupacao)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //    ProdutoDto produto = GetProduto(idProduto);
        //    if (produto.restricao.max == 0 && produto.restricao.min != 0)
        //    {
        //        return taxaOcupacao >= produto.restricao.min && taxaOcupacao <= 100;
        //    }

        //    if (produto.restricao.max != 0 && produto.restricao.min == 0)
        //    {
        //        return taxaOcupacao <= produto.restricao.max;
        //    }

        //    return taxaOcupacao >= produto.restricao.min && taxaOcupacao <= produto.restricao.max;

        //}

    }
}