﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArqsiArmario.Models;
using Microsoft.EntityFrameworkCore;

namespace ArqsiArmario.Repository
{
    public class MaterialRepository : IMaterialRepository, IDisposable
    {


        private ArqsiContext context;

        public MaterialRepository(ArqsiContext context)
        {
            this.context = context;
        }

        public void DeleteMaterial(int Materialid)
        {
            Material mat = context.Materiais.Find(Materialid);
            context.Materiais.Remove(mat);
        }
        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Material> GetMateriais()
        {
           IEnumerable<Material> mate=context.Materiais;
            
            foreach (var mat in context.Materiais) {
                var material =context.Materiais.Include(p => p.MaterialAcabamentos).Include(p => p.ProdutoMateriais).Where(p => p.Id == mat.Id).First();
                foreach(var aca in material.MaterialAcabamentos)
                {
                    aca.Acabamento = context.Acabamentos.Find(aca.AcabamentoId);

                }

                foreach(var pro in material.ProdutoMateriais)
                {
                    pro.Produto = context.Produtos.Find(pro.ProdutoId);

                }
                mate.Append(material);
            }
            return mate; }

        public Material GetMaterialByID(int MaterialId)
        {
            var Material = context.Materiais.Include(p => p.MaterialAcabamentos).Include(p => p.ProdutoMateriais).Where(p => p.Id == MaterialId).First();
            return Material;
        }

        public Material GetMaterialByID(int? MaterialId)
        {
            return context.Materiais.Find(MaterialId);
        }
        public void InsertMaterial(Material Material)
        {
            context.Materiais.Add(Material);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void UpdateMaterial(Material Material)
        {
            context.Entry(Material).State = EntityState.Modified;
        }
    }
}
