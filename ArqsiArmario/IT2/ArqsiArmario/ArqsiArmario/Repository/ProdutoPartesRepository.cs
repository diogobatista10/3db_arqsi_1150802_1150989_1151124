﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArqsiArmario.Models;
using Microsoft.EntityFrameworkCore;

namespace ArqsiArmario.Repository
{
    public class ProdutoPartesRepository : IProdutoPartesRepository, IDisposable
    {
        ArqsiContext context;
        public ProdutoPartesRepository(ArqsiContext context)
        {
            this.context = context;
        }
      

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void DeleteProdutoPartes(Produto ProdutoPai, Produto ProdutoFilho)
        {
            ProdutoPartes pro = context.ProdutoPartes.Find(ProdutoPai);
            ProdutoPartes pro2 = context.ProdutoPartes.Find(ProdutoFilho);
            context.ProdutoPartes.Remove(pro);
            context.ProdutoPartes.Remove(pro2);
        }
        
        public ProdutoPartes GetPartesByProduto(Produto ProdutoPai)
        {
            ProdutoPartes pro = context.ProdutoPartes.Find(ProdutoPai);
            return context.ProdutoPartes.Find(pro);
        }

        public ProdutoPartes GetProdutoByPartes(Produto ProdutoFilho)
        {
            ProdutoPartes pro = context.ProdutoPartes.Find(ProdutoFilho);
            return context.ProdutoPartes.Find(pro);
        }

        public IEnumerable<ProdutoPartes> GetProdutosPartes()
        {
            return context.ProdutoPartes;
        }

        public void InsertProdutoPartes(ProdutoPartes ProdutoPartes)
        {
            context.ProdutoPartes.Add(ProdutoPartes);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void UpdateProdutoPartes(ProdutoPartes ProdutoPartes)
        {
            context.Entry(ProdutoPartes).State = EntityState.Modified;
        }
    }
}
