﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArqsiArmario.Models;

namespace ArqsiArmario.Repository
{
    public interface IProdutoPartesRepository : IDisposable
    {
        IEnumerable<ProdutoPartes> GetProdutosPartes();
        ProdutoPartes GetPartesByProduto(Produto ProdutoPai);
        ProdutoPartes GetProdutoByPartes(Produto ProdutoFilho);
        void InsertProdutoPartes(ProdutoPartes ProdutoPartes);
        void DeleteProdutoPartes(Produto ProdutoPai, Produto ProdutoFilho);
        void UpdateProdutoPartes(ProdutoPartes ProdutoPartes);
        void Save();
    }
}