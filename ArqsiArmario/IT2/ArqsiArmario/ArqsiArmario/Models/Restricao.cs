﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArqsiArmario.DTOs;

namespace ArqsiArmario.Models
{
    public class Restricao
    {
        public float Min { get; set; }
        public float Max { get; set; }
        public bool MaterialAcabamento { get; set; }
        public bool ProdutoMaterial { get; set; }
        public Restricao() { }
        public Restricao(float Min, float Max, bool PM, bool MA)
        {
            this.Min = Min;
            this.Max = Max;
            this.ProdutoMaterial = PM;
            this.MaterialAcabamento = MA;
        }
        public RestricaoDto toDTO()
        {
            RestricaoDto restricaoDto = new RestricaoDto(this.Min,this.Max,this.ProdutoMaterial,this.MaterialAcabamento);
            return restricaoDto;
        }
        public static Restricao fromDTO(RestricaoDto restricaoDto)
        {
            
            Restricao restricao = new Restricao(restricaoDto.min, restricaoDto.max, restricaoDto.PM, restricaoDto.MA);
            return restricao;
        }
    }
}
