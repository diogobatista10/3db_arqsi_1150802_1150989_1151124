﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArqsiArmario.DTOs;

namespace ArqsiArmario.Models
{
    public class ProdutoPartes
    {
        public ProdutoPartes(Produto ProdutoPai, Produto ProdutoFilho)
        {
            this.ProdutoPai = ProdutoPai;
            this.ProdutoFilho = ProdutoFilho;
        }
        public ProdutoPartes() { }

        public int? ProdutoPaiId { get; set; }
        public Produto ProdutoPai { get; set; }

        public int? ProdutoFilhoId { get; set; }
        public Produto ProdutoFilho { get; set; }

        public ProdutoPartesDto toDTO()
        {
            ProdutoPartesDto produtoPartesDto = new ProdutoPartesDto(this.ProdutoPai.toDTO(), this.ProdutoFilho.toDTO());
            return produtoPartesDto;
        }
        public static ProdutoPartes fromDTO(ProdutoPartesDto produtoPartesDto)
        {
            ProdutoPartes produtoPartes = new ProdutoPartes(Produto.fromDTO(produtoPartesDto.ProdutoPai), Produto.fromDTO(produtoPartesDto.ProdutoFilho));
            return produtoPartes;
        }
        public override bool Equals(object obj)
        {
            var produtoPartes = obj as ProdutoPartes;
            return produtoPartes != null && this.ProdutoPaiId == produtoPartes.ProdutoPaiId && this.ProdutoFilhoId == produtoPartes.ProdutoFilhoId;
        }
    }
}
