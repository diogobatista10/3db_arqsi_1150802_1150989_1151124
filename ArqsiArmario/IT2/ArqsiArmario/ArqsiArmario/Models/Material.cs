using System.Collections.Generic;
using ArqsiArmario.DTOs;

namespace ArqsiArmario.Models
{
    public class Material
    {
      
        public int Id { get; set; }

        public string Nome { get; set; }

        public virtual ICollection<MaterialAcabamento> MaterialAcabamentos { get; set; }
       
        public virtual ICollection<ProdutoMaterial> ProdutoMateriais { get; set; }
        

        public Material() { }
        public Material(string Nome,ICollection<MaterialAcabamento> materialAcabamentos,ICollection<ProdutoMaterial> produtoMaterials)
        {
            this.Nome = Nome;
            this.MaterialAcabamentos = materialAcabamentos;
            this.ProdutoMateriais = produtoMaterials;
        }

        public MaterialDto toDTO()
        {
            List<MaterialAcabamentoDto> ListaMA = new List<MaterialAcabamentoDto>();
            foreach (MaterialAcabamento ma in this.MaterialAcabamentos)
            {
                ListaMA.Add(ma.toDTO());
            }
            List<ProdutoMaterialDto> ListaPM = new List<ProdutoMaterialDto>();
            foreach(ProdutoMaterial pm in this.ProdutoMateriais)
            {
                ListaPM.Add(pm.toDTO());
            }
            MaterialDto materialDto = new MaterialDto();
            materialDto.Nome = this.Nome;
            return materialDto;
        }
        public static Material fromDTO(MaterialDto MaterialDto)
        {
          /*  List<MaterialAcabamento> ListaMa = new List<MaterialAcabamento>();
            foreach(MaterialAcabamentoDto ma in MaterialDto.MaterialAcabamento)
            {
                ListaMa.Add(MaterialAcabamento.fromDTO(ma));
            }
            List<ProdutoMaterial> ListaPM = new List<ProdutoMaterial>();
            foreach(ProdutoMaterialDto pm in MaterialDto.ProdutoMaterial)
            {
                ListaPM.Add(ProdutoMaterial.fromDTO(pm));
            }*/
            Material material = new Material();
            material.Nome = MaterialDto.Nome;
            return material;
        }
        public override bool Equals(object obj)
        {
            var acabamento = obj as Acabamento;
            return acabamento != null && this.Id == acabamento.Id && EqualityComparer<string>.Default.Equals(Nome, acabamento.Nome);
        }
    }
}