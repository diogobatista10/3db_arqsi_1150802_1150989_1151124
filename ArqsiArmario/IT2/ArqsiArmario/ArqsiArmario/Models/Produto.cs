using System.Collections.Generic;
using ArqsiArmario.DTOs;

namespace ArqsiArmario.Models
{
    public class Produto
    {
        private Restricao restricao;
        public int Id { get; set; }
        public bool Composto { get; set; }
        public string Nome { get; set; }
        
        public virtual ICollection<ProdutoMaterial> ProdutosMateriais { get; set; }

        public virtual ICollection<ProdutoPartes> ProdutosPartes { get; set;}

        public int? CategoriaId { get; set; }
        public Categoria Categoria { get; set; }

        public int? DimensaoId { get; set; }
        public Dimensao Dimensao { get; set; }

        public Produto() { }
        public Produto(string Nome,bool Composto,ICollection<ProdutoPartes> Produtos, ICollection<ProdutoMaterial> ProdutoMateriais, Categoria Categoria,Dimensao Dimensao, Restricao restricao)
        {
            this.Nome = Nome;
            this.Composto = Composto;
            this.ProdutosPartes = Produtos;
            this.ProdutosMateriais = ProdutoMateriais;
            this.Categoria = Categoria;
            this.Dimensao = Dimensao;
            this.restricao = restricao;
        }

        public Produto(string Nome)
        {
            this.Nome = Nome;
        }

        public ProdutoDto toDTO()
        {
            List<ProdutoPartesDto> subProdutos = new List<ProdutoPartesDto>();
            List<ProdutoMaterialDto> materiais = new List<ProdutoMaterialDto>();

          /*  foreach(ProdutoPartes p in this.ProdutosPartes)
            {
                subProdutos.Add(p.toDTO());
            }*/
            /*foreach(ProdutoMaterial pm in this.ProdutosMateriais)
            {
                materiais.Add(pm.toDTO());
            }*/
       
            ProdutoDto produtoDto = new ProdutoDto(this.Nome,this.Composto,subProdutos,materiais,this.Categoria.toDTO(),this.restricao.toDTO());
            return produtoDto;
        }
        public static Produto fromDTO(ProdutoDto produtoDto)
        {
            List<ProdutoPartes> ListaP = new List<ProdutoPartes>();
            List<ProdutoMaterial> ListaPM = new List<ProdutoMaterial>();
            foreach (ProdutoMaterialDto pm in produtoDto.ProdutoMateriais)
            {
                ListaPM.Add(ProdutoMaterial.fromDTO(pm));
            }
            foreach(ProdutoPartesDto p in produtoDto.ProdutoPartes)
            {
                ListaP.Add(ProdutoPartes.fromDTO(p));
            }
            Produto produto = new Produto(produtoDto.Nome,produtoDto.Composto,ListaP,ListaPM,Categoria.fromDTO(produtoDto.Categoria),Dimensao.fromDTO(produtoDto.Dimensao),Restricao.fromDTO(produtoDto.restricao));
            return produto;
        }
        public override bool Equals(object obj)
        {
            var produto = obj as Acabamento;
            return produto != null && this.Id == produto.Id && EqualityComparer<string>.Default.Equals(Nome, produto.Nome);
        }
        private bool temMaterial(ProdutoPartes pp, Produto Produto)
        {
            foreach (ProdutoMaterial pm in pp.ProdutoFilho.ProdutosMateriais)
            {
                foreach (ProdutoMaterial pmp in Produto.ProdutosMateriais)
                {
                    if (pm.Material.Equals(pmp.Material))
                    {
                        if (restricao.MaterialAcabamento)
                        {
                            if (temAcabamento(pm, pmp))
                            {
                                return true;
                            }
                        }
                    }
                }

            }
            return false;
        }
        private bool temAcabamento(ProdutoMaterial pm, ProdutoMaterial pmp)
        {
            foreach (MaterialAcabamento ma in pm.Material.MaterialAcabamentos)
            {
                foreach (MaterialAcabamento map in pmp.Material.MaterialAcabamentos)
                {
                    if (ma.Acabamento.Equals(map.Acabamento))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private bool caber(Produto Produto)
        {
            bool auxB = true;
            float percentagem_ocupacao = 0;
            foreach (ProdutoPartes aux in Produto.ProdutosPartes)
            {
                float volumePai = Produto.Dimensao.Altura.AlturaMin * Produto.Dimensao.Largura.AlturaMin * Produto.Dimensao.Profundidade.AlturaMin;
                if ((aux.ProdutoFilho.Dimensao.Altura.AlturaMin < Produto.Dimensao.Altura.AlturaMin)
                    && (aux.ProdutoFilho.Dimensao.Largura.AlturaMin < Produto.Dimensao.Largura.AlturaMin)
                    && (aux.ProdutoFilho.Dimensao.Profundidade.AlturaMin < Produto.Dimensao.Profundidade.AlturaMin))
                {
                    float volumeFilho = aux.ProdutoFilho.Dimensao.Altura.AlturaMin * aux.ProdutoFilho.Dimensao.Largura.AlturaMin * aux.ProdutoFilho.Dimensao.Profundidade.AlturaMin;
                    percentagem_ocupacao += volumeFilho * 100 / volumePai;
                    if (percentagem_ocupacao > restricao.Min && percentagem_ocupacao < restricao.Max)
                    {
                        auxB = true;
                    }
                    else
                    {
                        auxB = false;
                    }
                }
                else
                {
                    auxB = false;
                }
            }
            return auxB;
        }
    }
}