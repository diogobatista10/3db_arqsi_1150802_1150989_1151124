const Encomenda = require('../models/encomenda.model');


exports.encomenda_create = function (req, res) {
    let encomenda = new Encomenda(
        {
            name: req.body.name,
            id: req.body.id,
            price: req.body.price,
            iva: req.body.iva,
            nif: req.body.nif,
            itens: req.body.itens,
            morada: req.body.morada,
            date: req.body.date
        }
    );


    encomenda.save(function (err) {
        if (err) {
            return console.log(err);
        }
        res.send('Encomenda Created successfully');
    })
};

exports.encomenda_details = function (req, res) {
    Encomenda.findById(req.params.id, function (err, encomenda) {
        if (err) return console.log(err);
        res.send(encomenda);
    })
};
exports.encomenda_details_itens = function (req, res) {
    Encomenda.findById(req.params.id, function (err, encomenda) {
        if (err) return console.log(err);
        res.send(encomenda.itens);
    })
    };
    //exports.encomenda_details_itens_item = function (req, res) {
    //    Encomenda.findById(req.params.id, function (err, encomenda) {
    //        if (err) return console.log(err);
    //        var enc = encomenda;
    //        ItemService.item_create();
    //        ItemService.findById(req.params.idP, function (err, item) {
    //            if (err) return console.log(err);
    //            res.send(item);
    //        })

    //        //})
    //        //ItemService.findById(enc.id)
    //        //    var final = Item.findById(req.params.id);

    //        //    Encomenda.findById(req.params.id, function (err, encomenda) {
    //        //        if (err) return console.log(err);
    //        //    //var item = encomenda.itens;
    //        //    //var index;
    //        //    //for (index = 0; index < item.length; ++index) {
    //        //    //    if (item[index].id == req.params.id){
    //        //    //        res.send(i);
    //        //    //    }
    //        //    //}
    //        //    res.send(final);
    //        //})
    //    })
    //};

    exports.encomenda_update = function (req, res) {
        Encomenda.findByIdAndUpdate(req.params.id, { $set: req.body }, function (err, Encomenda) {
            if (err) return console.log(err);
            res.send('Encomenda udpated.');
        });
    };

    exports.encomenda_delete = function (req, res) {
        Encomenda.findByIdAndRemove(req.params.id, function (err) {
            if (err) return console.log(err);
            res.send('Deleted successfully!');
        })
    }

    exports.encomenda_details_all = function (req, res) {
        Encomenda.find(function (err, encomenda) {
         if (err) return console.log(err);
         res.send(encomenda);
        })
    }