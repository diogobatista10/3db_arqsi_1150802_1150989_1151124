
// JavaScript source code
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//var idvalidator = require('mongoose-id-validator');
const itemRoute = require('../routes/item.route');

let ItemSchema = new Schema({
    // //_id: { type: String, required: true },
    produto: { type: Number, required: true},
    price: { type: Number, required: true },
    material: { type: Number, required: true },
    acabamento: { type: Number, required: true },
    //dimensao: { type: Number, required: true }


});
//ItemSchema.plugin(idvalidator);

ItemSchema.methods.toDTO = function () {

    return new itemDTO(this.data);
}

ItemSchema.methods.fromDTO = function (req, res) {
    return new itemDTO(this.data);
}


// Export the model

//ItemSchema.plugin(idvalidator);

module.exports = mongoose.model('Item', ItemSchema);


