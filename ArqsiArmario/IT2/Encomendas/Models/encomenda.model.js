// JavaScript source code
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//var idvalidator = require('mongoose-id-validator');

let EncomendaSchema = new Schema({
    name: { type: String, optional: true, max: 100 },
    nif: { type: Number, required: true },
    //_id: { type: String, required: true },
    price: { type: Number, required: true },
    itens: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Item' }],
    iva: { type: Number, required: true },
    morada: { type: String, optional: true, max: 150 },
    date: { type: Date, default:Date.now()}
});
//EncomendaSchema.plugin(idvalidator);

// Export the model

EncomendaSchema.methods.toDTO = function () {
    var itensDTO = [];
    for (var i = 0; i < this.itens.length; i++) {
        itensDTO.push(this.itens[i].toDTO());
    }

    return new encomendaDTO(itensDTO, this.data);
}
EncomendaSchema.methods.fromDTO = function (req, res) {
    var itens = [];
    for (var i = 0; i < this.itensDTO.length; i++) {
        itens.push(fromDTO(this.itensDTO[i]));
    }

    return new encomendaDTO(itens, this.data);
}

module.exports = mongoose.model('Encomenda', EncomendaSchema);




