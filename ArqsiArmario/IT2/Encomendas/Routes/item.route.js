const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const item_controller = require('../controllers/itemController');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test', item_controller.test);
router.post('/create', item_controller.item_create);
router.get('/:id', item_controller.item_details);
router.put('/:id/update', item_controller.item_update);
router.delete('/:id/delete', item_controller.item_delete);
router.get('/', item_controller.item_details_all);
module.exports = router;

//var Client = require('node-rest-client').Client;

//var client = new Client();

//exports.middleware_requests = function(id){
//    // direct way
//    client.get("https://arqsiarmario20181107040732.azurewebsites.net/api/produto/" + id, function (data, response) {
//        // parsed response body as js object
//        console.log(data);
//        // raw response
//        console.log(response);
//    })
//};
