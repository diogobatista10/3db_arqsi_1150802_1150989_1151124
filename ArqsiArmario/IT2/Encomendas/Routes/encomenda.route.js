
const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const encomenda_controller = require('../controllers/encomendaController');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test', encomenda_controller.test);
router.post('/create', encomenda_controller.encomenda_create);
router.get('/:id', encomenda_controller.encomenda_details);
router.put('/:id/update', encomenda_controller.encomenda_update);
router.delete('/:id/delete', encomenda_controller.encomenda_delete);
router.get('/:id/itens', encomenda_controller.encomenda_details_itens);
router.get('/:id/itens/:idP', encomenda_controller.encomenda_details_itens_item);
router.get('/', encomenda_controller.encomenda_details_all);

module.exports = router;

//var Client = require('node-rest-client').Client;

//var client = new Client();

//// direct way
//client.get("http://localhost:1991/encomendas/", function (data, response) {
//    // parsed response body as js object
//    console.log(data);
//    // raw response
//    console.log(response);
//});


