// JavaScript source code
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//var idvalidator = require('mongoose-id-validator');

let ItemSchema = new Schema({
    //produto: { type: mongoose.Schema.Types.ObjectId, ref: 'Produto' },
    //produtos: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Produto' }],
    produto: { type: Number, required: true },
    price: { type: Number, required: true },
    material: { type: Number, required: true },
    acabamento: { type: Number, required: true }

});
//Item.plugin(idvalidator);

// Export the model

//ItemSchema.plugin(idvalidator);
module.exports = mongoose.model('ItemDTO', ItemSchema);