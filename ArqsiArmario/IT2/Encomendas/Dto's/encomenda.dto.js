
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//var idvalidator = require('mongoose-id-validator');

let EncomendaSchema = new Schema({
    name: { type: String, optional: true, max: 100 },
    nif: { type: Number, required: true },
    price: { type: Number, required: true },
    itens: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Item' }],
    iva: { type: Number, required: true },
    morada: { type: String, optional: true, max: 150 },
    date: { type: Date, default: Date.now() }
});
//Encomenda.plugin(idvalidator);


data = new function () {
    var date = new Date();
    return date.getDay() + "-" + date.getMonth() + "-" + date.getFullYear();
}
// Export the model
module.exports = mongoose.model('EncomendaDTO', EncomendaSchema);