

const service = require('../services/encomendaServices');

//Simple version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the Test controller!');
};

exports.encomenda_create = function (req, res) {
    service.encomenda_create(req, res);
};

exports.encomenda_details = function (req, res) {
    service.encomenda_details(req, res);
};

exports.encomenda_update = function (req, res) {
    service.encomenda_update(req, res);
};

exports.encomenda_delete = function (req, res) {
    service.encomenda_delete(req, res);
}

exports.encomenda_details_itens = function (req, res) {
    service.encomenda_details_itens(req, res);
};

exports.encomenda_details_itens_item = function (req, res) {
    service.encomenda_details_itens_item(req, res);
};

exports.encomenda_details_all = function (req, res) {
    service.encomenda_details_all(req, res);
}