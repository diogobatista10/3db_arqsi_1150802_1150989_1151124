// JavaScript source code
const service = require('../services/itemServices');

//Simple version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the Test controller!');
};

exports.item_create = function (req, res) {
    service.item_create(req, res);
};

exports.item_details = function (req, res) {
    service.item_details(req, res);
};

exports.item_update = function (req, res) {
    service.item_update(req, res);
};
exports.item_delete = function (req, res) {
    service.item_delete(req, res);
};
exports.item_details_all = function (req, res) {
    service.item_details_all(req, res);
};

//exports.validarComponentes = function (req) {
//    service.validarComponentes(req);
//};