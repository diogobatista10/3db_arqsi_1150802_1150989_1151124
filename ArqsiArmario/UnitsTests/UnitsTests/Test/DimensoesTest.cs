using System;
using System.Collections.Generic;
using Xunit;
using UnitsTests.Models;

namespace UnitsTests
{
    public class DimensoesTest
    {
        [Fact]
        public void HasIdTest()
        {
            int id = 1;

            Dimensao d = new Dimensao();

            d.Id = id;

            Assert.Equal(id, d.Id);
        }

        [Fact]
        public void HasLarguraTest()
        {
            int Largura = 3;

            Dimensao d = new Dimensao();

            d.Larguraid = Largura;

            Assert.Equal(Largura, d.Larguraid);
        }

        [Fact]
        public void HasProfundidadeTest()
        {
            int profundidade = 3;

            Dimensao d = new Dimensao();

            d.Profundidadeid = profundidade;

            Assert.Equal(profundidade, d.Profundidadeid);
        }

    }

}