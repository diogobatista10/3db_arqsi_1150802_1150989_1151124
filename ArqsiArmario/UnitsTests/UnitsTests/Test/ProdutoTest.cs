using System;
using System.Collections.Generic;
using Xunit;
using UnitsTests.Models;

namespace UnitsTests
{
    public class ProdutoTest
    {
        [Fact]
        public void HasIdTest()
        {
            int id = 1;

            Produto p = new Produto();

            p.Id = id;

            Assert.Equal(id, p.Id);
        }

        [Fact]
        public void HasNomeProdutoTest()
        {
            string NomeProduto = "NomeProduto";

            Produto p = new Produto();

            p.Nome = NomeProduto;

            Assert.Equal(NomeProduto, p.Nome);
        }

        

    /*    [Fact]
        public void HasMaterialIdTest()
        {
            int MaterialId = 1;

            Produto p = new Produto();

            foreach (var promat in p.ProdutosMateriais)
            {

                promat.MaterialId = MaterialId;



                Assert.Equal(MaterialId, promat.MaterialId);
            }
            }

        [Fact]
        public void HasMaterialTest()
        {
            Material Material = new Material();

            Produto p = new Produto();


            foreach (var promat in p.ProdutosMateriais)
            {

                promat.Material = Material;


                Assert.Equal(Material, promat.Material);
            }
        }*/
        [Fact]
        public void HasCategoriaIdTest()
        {
            int CategoriaId = 1;

            Produto p = new Produto();

            p.CategoriaId = CategoriaId;

            Assert.Equal(CategoriaId, p.CategoriaId);
        }

        [Fact]
        public void HasCategoriaTest()
        {
            Categoria Categoria = new Categoria();

            Produto p = new Produto();

            p.Categoria = Categoria;

            Assert.Equal(Categoria, p.Categoria);
        }

        [Fact]
        public void HasDimensoesIdTest()
        {
            int DimensoesId = 1;

            Produto p = new Produto();

            p.DimensaoId = DimensoesId;

            Assert.Equal(DimensoesId, p.DimensaoId);
        }

        [Fact]
        public void HasDimensoesTest()
        {
            Dimensao Dimensoes = new Dimensao();

            Produto p = new Produto();

            p.Dimensao = Dimensoes;

            Assert.Equal(Dimensoes, p.Dimensao);
        }

    }

}