using System;
using System.Collections.Generic;
using Xunit;
using UnitsTests.Models;

namespace UnitsTests
{
    public class CategoriaTest
    {
        [Fact]
        public void HasIdTest()
        {
            int id = 1;

            Categoria c = new Categoria();

            c.CategoriaId = id;

            Assert.Equal(id, c.CategoriaId);
        }

        [Fact]
        public void HasNomeCategoriaTest()
        {
            string NomeCategoria = "nomeCategoria";

            Categoria c = new Categoria();

            c.Nome = NomeCategoria;

            Assert.Equal(NomeCategoria, c.Nome);
        }

        [Fact]
        public void HasIdSubCategoriasTest()
        {
            ICollection<Categoria> list = new List<Categoria>();

            Categoria c = new Categoria();

            list.Add(c);

            c.SubCategorias = list;

            Assert.NotEmpty(c.SubCategorias);
        }

    }

}