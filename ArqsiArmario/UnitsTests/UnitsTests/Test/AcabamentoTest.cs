﻿using UnitsTests.Models;
using Xunit;


namespace UnitsTests
{
    public class AcabamentoTest
    {
        [Fact]
        public void HasIdTest()
        {
            int id = 1;

            Acabamento a = new Acabamento();

            a.Id = id;

            Assert.Equal(id, a.Id);
        }

        [Fact]
        public void HasDescricaoTest()
        {
            string nome = "descricao";

            Acabamento a = new Acabamento();

            a.Nome = nome;

            Assert.Equal(nome, a.Nome);
        }

    }

}