using System;
using System.Collections.Generic;
using Xunit;
using UnitsTests.Models;

namespace UnitsTests
{
    public class MaterialTest
    {
        [Fact]
        public void HasIdTest()
        {
            int id = 1;

            Material m = new Material();

            m.Id = id;

            Assert.Equal(id, m.Id);
        }

        [Fact]
        public void HasNomeMaterialTest()
        {
            string NomeMaterial = "NomeMaterial";

            Material m = new Material();

            m.Nome = NomeMaterial;

            Assert.Equal(NomeMaterial, m.Nome);
        }

        [Fact]
        public void HasIdAcabamentosTest()
        {
            List<MaterialAcabamento> list = new List<MaterialAcabamento>();
            Material m = new Material();
            MaterialAcabamento ma = new MaterialAcabamento();

            list.Add(ma);

            m.MaterialAcabamentos = list;
           


                Assert.NotEmpty(m.MaterialAcabamentos);
            
        }

    }

}