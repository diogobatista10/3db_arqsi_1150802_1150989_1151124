﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace UnitsTests.Models
{
    public class ProdutoPartes
    {
        public ProdutoPartes(Produto ProdutoPai, Produto ProdutoFilho)
        {
            this.ProdutoPai = ProdutoPai;
            this.ProdutoFilho = ProdutoFilho;
        }
        public ProdutoPartes() { }

        public int? ProdutoPaiId { get; set; }
        public Produto ProdutoPai { get; set; }

        public int? ProdutoFilhoId { get; set; }
        public Produto ProdutoFilho { get; set; }

        
        public override bool Equals(object obj)
        {
            var produtoPartes = obj as ProdutoPartes;
            return produtoPartes != null && this.ProdutoPaiId == produtoPartes.ProdutoPaiId && this.ProdutoFilhoId == produtoPartes.ProdutoFilhoId;
        }
    }
}
