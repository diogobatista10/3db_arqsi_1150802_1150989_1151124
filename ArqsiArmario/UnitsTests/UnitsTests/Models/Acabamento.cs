using System.Collections.Generic;


namespace UnitsTests.Models
{
    public class Acabamento
    {



        public int Id { get; set; }
        public string Nome { get; set; }

       public ICollection<MaterialAcabamento> MaterialAcabamentos { get; set; }
        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
      public  ICollection<int?> MaterialAcabamentosId { get; set; }

        public Acabamento(string Nome) { this.Nome = Nome; }
        public Acabamento()
        {
        }

       
        public override bool Equals(object obj)
        {
            var acabamento = obj as Acabamento;
            return acabamento != null && this.Id == acabamento.Id && EqualityComparer<string>.Default.Equals(Nome, acabamento.Nome);
        }
    }
}