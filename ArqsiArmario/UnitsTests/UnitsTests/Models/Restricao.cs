﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UnitsTests.Models
{
    public class Restricao
    {
        public float Min { get; set; }
        public float Max { get; set; }
        public bool MaterialAcabamento { get; set; }
        public bool ProdutoMaterial { get; set; }
        public Restricao() { }
        public Restricao(float Min, float Max, bool PM, bool MA)
        {
            this.Min = Min;
            this.Max = Max;
            this.ProdutoMaterial = PM;
            this.MaterialAcabamento = MA;
        }
        
    }
}
