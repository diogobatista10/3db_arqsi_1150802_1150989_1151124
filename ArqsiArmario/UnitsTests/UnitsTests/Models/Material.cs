using System.Collections.Generic;

namespace UnitsTests.Models
{
    public class Material
    {
      
        public int Id { get; set; }

        public string Nome { get; set; }

        public virtual ICollection<MaterialAcabamento> MaterialAcabamentos { get; set; }
       
        public virtual ICollection<ProdutoMaterial> ProdutoMateriais { get; set; }
        

        public Material() { }
        public Material(string Nome,ICollection<MaterialAcabamento> materialAcabamentos,ICollection<ProdutoMaterial> produtoMaterials)
        {
            this.Nome = Nome;
            this.MaterialAcabamentos = materialAcabamentos;
            this.ProdutoMateriais = produtoMaterials;
        }

       
        public override bool Equals(object obj)
        {
            var acabamento = obj as Acabamento;
            return acabamento != null && this.Id == acabamento.Id && EqualityComparer<string>.Default.Equals(Nome, acabamento.Nome);
        }
    }
}