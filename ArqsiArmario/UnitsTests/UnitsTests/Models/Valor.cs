﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UnitsTests.Models
{
    public class Valor
    {
        public int Id { get; set; }
        public int ValorDiscreto { get; set; }

        public Valor() { }
        public Valor(int ValorDiscreto)
        {
            this.ValorDiscreto = ValorDiscreto;
        }
      
        public override bool Equals(object obj)
        {
            var valor = obj as Valor;
            return valor != null && this.Id == valor.Id;
        }
    }
}
