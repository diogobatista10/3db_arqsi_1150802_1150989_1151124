﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace UnitsTests.Models
{
    public class DimensaoDC
    {
        public DimensaoDC() { }
        public DimensaoDC(float AlturaMin, float AlturaMax, ICollection<Valor> ListaDiscreta)
        {
            this.AlturaMin = AlturaMin;
            this.AlturaMax = AlturaMax;
        }
        public int Id { get; set; }
        public ICollection<Valor> ListaDiscreta { get; set; }
        public float AlturaMin { get; set; }
        public float AlturaMax { get; set; }


      
    }
}
