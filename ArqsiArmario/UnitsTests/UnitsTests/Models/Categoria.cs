using System.Collections;
using System.Collections.Generic;


namespace UnitsTests.Models
{
    public class Categoria
    {
        public Categoria(string Nome,string Descricao, bool Composto, Categoria CategoriaPai, ICollection<Categoria> SubCategorias)
        {
            this.Nome = Nome;
            this.CategoriaPai = CategoriaPai;
            this.Descricao = Descricao;
            this.Composto = Composto;
            this.CategoriaPai = CategoriaPai;
            this.SubCategorias = SubCategorias;
        }
        public Categoria() { }
        public int Id { get; set; }
        public bool Composto { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public virtual Categoria CategoriaPai { get; set; }
        public int? CategoriaId { get; set; }

        [System.ComponentModel.DataAnnotations.Schema.NotMapped]
        public ICollection<int?> SubCategoriasId { get; set; }
        public ICollection<Categoria> SubCategorias { get; set; }

       
        public override bool Equals(object obj)
        {
            var acabamento = obj as Categoria;
            return acabamento != null && this.Id == acabamento.Id && EqualityComparer<string>.Default.Equals(Nome, acabamento.Nome);
        }

    }
}