﻿

namespace UnitsTests.Models
{
    public class ProdutoMaterial
    {
        public ProdutoMaterial(Produto Produto, Material Material)
        {
            this.Produto = Produto;
            this.MaterialId = Material.Id;
            this.ProdutoId = Produto.Id;
            this.Material = Material;
        }
        public ProdutoMaterial() { }

        public int ProdutoId { get; set; }
        public Produto Produto { get; set; }

        public int MaterialId { get; set; }
        public Material Material { get; set; }


      
        public override bool Equals(object obj)
        {
            var produtoMaterial = obj as ProdutoMaterial;
            return produtoMaterial != null && this.ProdutoId == produtoMaterial.ProdutoId && this.MaterialId == produtoMaterial.MaterialId ;
        }
    }
}
