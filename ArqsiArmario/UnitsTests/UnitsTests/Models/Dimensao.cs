using System.Collections.Generic;


namespace UnitsTests.Models
{
    public class Dimensao
    {
        public Dimensao() { }
        public Dimensao(DimensaoDC Altura, DimensaoDC Largura, DimensaoDC Profundidade)
        {
            this.Altura = Altura;
            this.Largura = Largura;
            this.Profundidade = Profundidade;
        }
        public int Id { get; set; }

        public int alturaid { get; set; }
        public DimensaoDC Altura { get; set; }
        public int Profundidadeid { get; set; }
        public DimensaoDC Profundidade { get; set; }
        public int Larguraid { get; set; }
        public DimensaoDC Largura { get; set; }


      
        public override bool Equals(object obj)
        {
            var dimensao = obj as Dimensao;
            return dimensao != null && this.Id == dimensao.Id;
        }
    }
}